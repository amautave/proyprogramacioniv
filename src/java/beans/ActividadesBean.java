/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dao.ActividadesDaoImpl;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Actividades;
import model.Usuario;

/**
 *
 * @author Victor
 */
@ManagedBean
@SessionScoped
public class ActividadesBean {

    private Integer id;
    private Usuario usuario;
    private Date fechaHoraInicio;
    private Date fechaHoraFin;
    private String descripcion;
    private Boolean esSesion;
    private List<Actividades> lstActividades;
    ActividadesDaoImpl daoImpl;
    Actividades oActividades;
        
    /**
     * Creates a new instance of ActividadesBean
     */
    public ActividadesBean() {
        daoImpl = new ActividadesDaoImpl();
        lstActividades = obtenerListActividadesDao();
        oActividades = new Actividades();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getFechaHoraInicio() {
        return fechaHoraInicio;
    }

    public void setFechaHoraInicio(Date fechaHoraInicio) {
        this.fechaHoraInicio = fechaHoraInicio;
    }

    public Date getFechaHoraFin() {
        return fechaHoraFin;
    }

    public void setFechaHoraFin(Date fechaHoraFin) {
        this.fechaHoraFin = fechaHoraFin;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEsSesion() {
        return esSesion;
    }

    public void setEsSesion(Boolean esSesion) {
        this.esSesion = esSesion;
    }

    public List<Actividades> getLstActividades() {
        return lstActividades;
    }

    public void setLstActividades(List<Actividades> lstActividades) {
        this.lstActividades = lstActividades;
    }
    
    private List<Actividades> obtenerListActividadesDao(){
        return daoImpl.getActividades();
    }
    
    public void Guardar(){
        try{
            oActividades.setDescripcion(descripcion);
            oActividades.setEsSesion(esSesion);
            oActividades.setFechaHoraFin(fechaHoraFin);
            oActividades.setFechaHoraInicio(fechaHoraInicio);
            oActividades.setUsuario(usuario);
            
            daoImpl.create(oActividades);
            FacesMessage msg = new FacesMessage("Se ha guardado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);

            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/actividades/consultar.xhtml");
        }
        catch(Exception e){
            throw e;
        }
    }
}
