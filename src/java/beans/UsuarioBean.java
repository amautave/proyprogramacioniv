/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dao.PerfilDaoImpl;
import dao.UsuarioDaoImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Perfil;
import model.Usuario;

/**
 *
 * @author Victor
 */
@ManagedBean
@SessionScoped
public class UsuarioBean {
    
    private Integer id;
    private Perfil perfil;
    private String numId;
    private String nombres;
    private String apellidos;
    private String correo;
    private Set actividadeses = new HashSet(0);
    private Usuario oUsuario;
    private Usuario item;
    private List<Perfil> lstPerfiles;
    private List<Usuario> lstUsuarios;
    public String sIdSelect;
    UsuarioDaoImpl daoImpl;
    PerfilDaoImpl oPerFilDaoImpl;

    /**
     * Creates a new instance of UsuarioBean
     */
    public UsuarioBean() {
        oUsuario = new Usuario();
        item = new Usuario();
        daoImpl = new UsuarioDaoImpl();
        oPerFilDaoImpl = new PerfilDaoImpl();
        lstPerfiles = ObtenerDaoListPerfiles();
        lstUsuarios = obtenerDaoListUsuarios();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Set getActividadeses() {
        return actividadeses;
    }

    public void setActividadeses(Set actividadeses) {
        this.actividadeses = actividadeses;
    }

    public Usuario getoUsuario() {
        return oUsuario;
    }

    public void setoUsuario(Usuario oUsuario) {
        this.oUsuario = oUsuario;
    }

    public Usuario getItem() {
        return item;
    }

    public void setItem(Usuario item) {
        this.item = item;
    }

    public List<Perfil> getLstPerfiles() {
        return lstPerfiles;
    }

    public void setLstPerfiles(List<Perfil> lstPerfiles) {
        this.lstPerfiles = lstPerfiles;
    }

    public List<Usuario> getLstUsuarios() {
        return lstUsuarios;
    }

    public void setLstUsuarios(List<Usuario> lstUsuarios) {
        this.lstUsuarios = lstUsuarios;
    }

    public String getsIdSelect() {
        return sIdSelect;
    }

    public void setsIdSelect(String sIdSelect) {
        this.sIdSelect = sIdSelect;
    }
    
    private List<Perfil> ObtenerDaoListPerfiles(){
        return oPerFilDaoImpl.getPerfiles();
    }
    
    private List<Usuario> obtenerDaoListUsuarios(){
        return daoImpl.getUsuarios();
    }
    
    private void getSelectedId(){
        if(this.sIdSelect != null){
            for(int i=0; i<this.lstPerfiles.size(); i++){
                if(this.sIdSelect.equals(this.lstPerfiles.get(i).getId().toString())){
                    perfil = this.lstPerfiles.get(i);
                    break;
                }
            }
        }
    }
    
    public void Guardar(){
        try{
            getSelectedId();

            oUsuario.setActividadeses(actividadeses);
            oUsuario.setApellidos(apellidos);
            oUsuario.setCorreo(correo);
            oUsuario.setNombres(nombres);
            oUsuario.setNumId(numId);
            oUsuario.setPerfil(perfil);

            daoImpl.create(oUsuario);
            FacesMessage msg = new FacesMessage("Se ha guardado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);

            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/usuarios/consultar.xhtml");
                        
            setLstUsuarios(obtenerDaoListUsuarios());
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public void Actualizar(){
        try{            
            getSelectedId();

            oUsuario = item;
            oUsuario.setPerfil(perfil);

            daoImpl.update(oUsuario);
            FacesMessage msg = new FacesMessage("Se ha actualizado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            setLstUsuarios(obtenerDaoListUsuarios());
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public void Eliminar(){
        try{
            getSelectedId();

            oUsuario = item;
            oUsuario.setPerfil(perfil);

            daoImpl.delete(oUsuario);
            FacesMessage msg = new FacesMessage("Se ha eliminado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            setLstUsuarios(obtenerDaoListUsuarios());
        }
        catch(Exception e){
            throw e;
        }
    }
}
