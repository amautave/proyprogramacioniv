/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dao.PerfilDaoImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Perfil;

/**
 *
 * @author Victor
 */
@ManagedBean
@SessionScoped
public class PerfilBean {
    
    private Integer id;
    private String nombre;
    private String descripcion;
    private Set usuarios = new HashSet(0);
    private Perfil oPerfil;
    private Perfil item;
    private List<Perfil> lstPerfiles;
    PerfilDaoImpl daoImpl;

    /**
     * Creates a new instance of PerfilBean
     */
    public PerfilBean() {
        oPerfil = new Perfil();
        item = new Perfil();
        daoImpl = new PerfilDaoImpl();
        lstPerfiles = obtenerListPerfiles();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set usuarios) {
        this.usuarios = usuarios;
    }

    public Perfil getoPerfil() {
        return oPerfil;
    }

    public void setoPerfil(Perfil oPerfil) {
        this.oPerfil = oPerfil;
    }

    public Perfil getItem() {
        return item;
    }

    public void setItem(Perfil item) {
        this.item = item;
    }

    public List<Perfil> getLstPerfiles() {
        return lstPerfiles;
    }

    public void setLstPerfiles(List<Perfil> lstPerfiles) {
        this.lstPerfiles = lstPerfiles;
    }

    private List<Perfil> obtenerListPerfiles(){
        return daoImpl.getPerfiles();
    }
    
    public void Guardar(){
        try{
            oPerfil.setDescripcion(descripcion);
            oPerfil.setNombre(nombre);
            oPerfil.setUsuarios(usuarios);

            daoImpl.create(oPerfil);
            FacesMessage msg = new FacesMessage("Se ha guardado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);

            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/perfiles/consultar.xhtml");
            
            setLstPerfiles(obtenerListPerfiles());
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public void Actualizar(){
        try{
            oPerfil = item;
            
            daoImpl.update(oPerfil);
            FacesMessage msg = new FacesMessage("Se ha actualizado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            setLstPerfiles(obtenerListPerfiles());
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public void Eliminar(){
        try{
            oPerfil = item;
            
            daoImpl.delete(oPerfil);
            FacesMessage msg = new FacesMessage("Se ha eliminado en base de datos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            setLstPerfiles(obtenerListPerfiles());
        }
        catch(Exception e){
            throw e;
        }
    }
}
