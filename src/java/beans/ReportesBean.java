/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dao.UsuarioDaoImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import model.Actividades;
import model.Usuario;
import reports.FechaHoras;

/**
 *
 * @author Andres Tabares
 */
@ManagedBean
@RequestScoped
public class ReportesBean {

    private Usuario usuario;
    private Date fechaIni;
    private Date fechaFin;
    private List<FechaHoras> listaFechaHoras;
    private List<Actividades> listaActividades;
    private List<SelectItem> listaUsuarios;

    /**
     * Creates a new instance of ReporteHorasBean
     */
    public ReportesBean() {
        usuario = new Usuario();
//        listaUsuarios = new ArrayList<>();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<FechaHoras> getListaFechaHoras() {
        return listaFechaHoras;
    }

    public void setListaFechaHoras(List<FechaHoras> listaFechaHoras) {
        this.listaFechaHoras = listaFechaHoras;
    }

    public List<Actividades> getListaActividades() {
        return listaActividades;
    }

    public void setListaActividades(List<Actividades> listaActividades) {
        this.listaActividades = listaActividades;
    }

    public List<SelectItem> getListaUsuarios() {

        listaUsuarios = new ArrayList<>();
        listaUsuarios.clear();

        UsuarioDaoImpl usuarioDao = new UsuarioDaoImpl();
        List<Usuario> lUsu = usuarioDao.findAll();

        lUsu.stream().forEach((usu) -> {
            listaUsuarios.add(new SelectItem(
                    String.valueOf(usu.getId()),
                    usu.getApellidos() + " " + usu.getNombres()));
        });

        return listaUsuarios;
    }

    public void setListaUsuarios(List<SelectItem> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String generarReporteHoras() {
        String rutaRepHoras = "/faces/reportes/reportesHoras";
        return rutaRepHoras;
    }

}
