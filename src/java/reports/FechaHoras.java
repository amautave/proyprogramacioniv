/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import java.util.Date;

/**
 *
 * @author Andres Tabares
 */
public class FechaHoras {

    private Date fecha;
    private double horas;

    public FechaHoras(Date fecha, double horas) {
        this.fecha = fecha;
        this.horas = horas;
    }

    public FechaHoras() {
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getHoras() {
        return horas;
    }

    public void setHoras(double horas) {
        this.horas = horas;
    }

}
