/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanSession;

import dao.UsuarioDaoImpl;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.Usuario;

/**
 *
 * @author Andres Tabares
 */
@ManagedBean
@SessionScoped
public class LoginSessionBean {

    private String correo;
    private String contrasena;

    /**
     * Creates a new instance of LoginSessionBean
     */
    public LoginSessionBean() {
        HttpSession miSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        miSession.setMaxInactiveInterval(600 /*seg*/);
    }

    public String login() {
        UsuarioDaoImpl usuarioDao = new UsuarioDaoImpl();
        Usuario usuario = usuarioDao.loginUsuario(correo, contrasena);
        if (usuario != null) {
            return "/bienvenida";
        }

        this.correo = null;
        this.contrasena = null;

        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de acceso:", "Usuario o Contraseña incorrecta"));
        return "/index";
    }

    public String salir() {
        this.correo = null;
        this.contrasena = null;
        return "/index";
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

}
