/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Perfil;
import org.hibernate.HibernateException;
import util.HibernateUtil;

/**
 *
 * @author Victor
 */
public class PerfilDaoImpl extends GenericDaoImpl<Perfil,Integer> implements PerfilDao {
    
    public List <Perfil> getPerfiles(){
        
         List <Perfil> list;
        try {
            session = HibernateUtil.getSession();
            tx = session.beginTransaction();
            list = session.createQuery("from Perfil p").list();
            tx.commit();
        } catch (HibernateException e) {
            list = null ;
            tx.rollback();
            throw e;
        }
        return list;
    }
}
