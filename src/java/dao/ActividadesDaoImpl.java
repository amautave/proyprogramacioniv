/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Actividades;
import org.hibernate.HibernateException;
import util.HibernateUtil;

/**
 *
 * @author Victor
 */
public class ActividadesDaoImpl extends GenericDaoImpl<Actividades,Integer> implements ActividadesDao {
    
    public List <Actividades> getActividades(){
        
         List <Actividades> list;
        try {
            session = HibernateUtil.getSession();
            tx = session.beginTransaction();
            list = session.createQuery("from Actividades p").list();
            tx.commit();
        } catch (HibernateException e) {
            list = null ;
            tx.rollback();
            throw e;
        }
        return list;
    }
}
