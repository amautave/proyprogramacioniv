/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Usuario;
import org.hibernate.HibernateException;
import util.HibernateUtil;

/**
 *
 * @author Victor
 */
public class UsuarioDaoImpl extends GenericDaoImpl<Usuario, Integer> implements UsuarioDao {

    public List<Usuario> getUsuarios() {

        List<Usuario> list;
        try {
            session = HibernateUtil.getSession();
            tx = session.beginTransaction();
            list = session.createQuery("from Usuario p").list();
            tx.commit();
        } catch (HibernateException e) {
            list = null;
            tx.rollback();
            throw e;
        }
        return list;
    }

    public Usuario loginUsuario(String correo, String contrasena) {
        Usuario usuario;
        try {
            session = HibernateUtil.getSession();
            tx = session.beginTransaction();
            usuario = (Usuario) session.createQuery("from Usuario p where p.correo = '" + correo + "'").uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            usuario = null;
            tx.rollback();
            throw e;
        }
        return usuario;
    }
}
